#!/usr/bin/env ruby

require 'sinatra'

get '/' do
  send_file File.join(settings.public_folder, 'index.html')
end

get '/preview' do
  send_file File.join(settings.public_folder, 'preview.html')
end

post '/register' do
  redirect '/preview'
end
