FROM ruby:onbuild

ENV RACK_ENV production

EXPOSE 4567

CMD ["./avium.rb"]
