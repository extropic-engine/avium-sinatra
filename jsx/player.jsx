Parse.initialize('WgmeA12ET8OYVZSzhBE8k1hDxwmeNjv5RvII20vH', 'YCoITunwLprdLiucsSRYirTuefcMYpF1fe0rqeBC');
SC.initialize({client_id: 'a9375359e3a389f05ae21d1796804b57'});
Parse.Analytics.track('view', {page: pageName});

//
// Player
//

var Player = React.createClass({
  mixins: [ParseReact.Mixin],

  getInitialState: function() {
    return {
      currentTrack: 0,
      playing: true,
      loading: false,
      sounds: {},
      currentSound: null,
    };
  },

  componentDidMount: function() {
    this.autoplay();

    $(document).keypress(function(e) {
      if (e.keyCode == 39) {
        e.preventDefault();
        this.actionNext();
      } else if (e.keyCode == 37) {
        e.preventDefault();
        this.actionPrev();
      } else if (e.keyCode == 38) {
        e.preventDefault();
        this.actionPrev();
      } else if (e.keyCode == 40) {
        e.preventDefault();
        this.actionNext();
      } else if (e.keyCode == 0) {
        if (e.key == ' ') {
          e.preventDefault();
          this.togglePlay();
        }
      }
    }.bind(this));
  },

  observe: function() {
    var playlistQuery = (new Parse.Query('Playlist')).equalTo('objectId', playlistId).include('creator').include('songs.artist');
    var Song = Parse.Object.extend('Song');
    var s = {};
    if (this.state.currentSongId) {
      s = new Song();
      s.id = this.state.currentSongId;
    }
    return {
      playlist: playlistQuery,
      review: (new Parse.Query('Review')).equalTo('song', s).include('reviewer')
    };
  },

  autoplay: function() {
    if (this.state.playing == true && this.state.currentSound == null) {
      if (this.data.playlist.length > 0) {
        this.playTrack(this.state.currentTrack);
      } else {
        setTimeout(this.autoplay, 200);
      }
    }
  },

  loadSound: function(trackId, callback) {
    var self = this;
    console.log('loading track ' + trackId);
    SC.stream("/tracks/" + trackId, {
      ontimeout: function() {
        console.log('track ' + trackId + ' timed out');
      },
      onload: function(success) {
        if (!success) {
          console.log('track failed to load, skipping');
          self.actionNext();
        }
      }
    }, function(sound) {
      console.log('loaded track ' + trackId);
      sounds = this.state.sounds;
      if (sounds[trackId]) {
        console.log('track ' + trackId + ' was already loaded, destroying');
        sounds[trackId].destroy();
        sounds[trackId] = null;
      }
      sounds[trackId] = sound;
      this.setState({ sounds: sounds });
      callback(sound);
    }.bind(this));
  },

  stopSounds: function() {
    $.each(this.state.sounds, function(i, v) {
      if (v) {
        console.log('ceasing sound ' + i);
        v.stop();
      }
    });
  },

  playTrack: function(trackIndex) {
    var currentTrackId = this.data.playlist[0].songs[this.state.currentTrack].soundcloudId;
    this.stopSounds();

    if (trackIndex < 0 || trackIndex >= this.data.playlist[0].songs.length) {
      console.log('trackIndex ' + trackIndex + ' not in playlist, stopping');
      this.setState({ currentTrack: 0, playing: false, currentSongId: null });
      return false;
    }

    var song = this.data.playlist[0].songs[trackIndex];
    this.playSound(song.soundcloudId);
    this.setState({ currentTrack: trackIndex, playing: true, currentSongId: song.objectId });
  },

  playSound: function(trackId) {
    var parameters = {
      onfinish: this.actionNext,
      whileplaying: this.whilePlaying,
    };
    if (this.state.sounds[trackId] == null) {
      this.loadSound(trackId, function(sound) {
        Parse.Analytics.track('play', {track: trackId});
        console.log('playing sound ' + trackId);
        sound.play(parameters);
      });
    } else {
      this.state.sounds[trackId].play(parameters);
    }
  },

  whilePlaying: function() {
    var sound = this.state.sounds[this.data.playlist[0].songs[this.state.currentTrack].soundcloudId];
    // TODO: rather than using durationEstimate we can store the exact duration in Parse and retrieve it from there
    this.setState({position: (sound.position / sound.durationEstimate) * 100});
  },

  togglePlay: function() {
    var currentTrackId = this.data.playlist[0].songs[this.state.currentTrack].soundcloudId;

    if (this.state.playing) {
      this.state.sounds[currentTrackId].pause();
    } else {
      this.state.sounds[currentTrackId].play();
    }

    this.setState({playing: !this.state.playing});
  },

  actionPrev: function() {
    this.playTrack(this.state.currentTrack - 1);
  },

  actionNext: function() {
    console.log('triggering actionNext');
    this.playTrack(this.state.currentTrack + 1);
  },

  render: function() {
    var playlist = this.data.playlist.length > 0 ? this.data.playlist[0] : { name: 'No Playlist', creator: { name: 'No Creator' }, songs: []};
    var track = playlist.songs.length > 0 ? playlist.songs[this.state.currentTrack] : {title: 'Loading...', artist: ''};
    var review = this.data.review.length > 0 ? this.data.review[0] : {reviewer: { avatar: { url: function() {}}}, text: ''};

    var playlistPanelProps = {
      playlist: playlist,
      currentTrack: this.state.currentTrack,
      changeTrackCallback: this.playTrack,
    };

    var reviewPanelProps = {
      reviewer: review.reviewer,
      review: review,
    };

    var controlBarProps = {
      actionPrev: this.actionPrev,
      actionNext: this.actionNext,
      togglePlay: this.togglePlay,
      track: track,
      playing: this.state.playing,
      position: this.state.position,
    };
    return (
      <div id="ContentContainer">
        <div id="PanelContainer">
          <PlaylistPanel o={playlistPanelProps} />
          <ReviewPanel o={reviewPanelProps} />
        </div>
        <ControlsBar o={controlBarProps} />
      </div>
    );
  }
});

//
// PlaylistPanel
//

var PlaylistPanel = React.createClass({

  selectTrack: function(e) {
    var selectedTrack = parseInt(e.currentTarget.getAttribute('data-track-number'));
    if (this.props.o.currentTrack != selectedTrack) {
      this.props.o.changeTrackCallback(selectedTrack);
    }
  },

  render: function() {
    return (
      <div id="PlaylistPanel">
        <div id="PlaylistHeader">
          <a href="http://avium.co" id="LogoWrapper"><img src="img/header.png" /></a>
          <h1 id="HeaderTitle">{this.props.o.playlist.name}</h1>
        </div>
        {this.props.o.playlist.songs.map(function(s,i) {
          var rowClasses = 'playlist-row ';
          if (this.props.o.currentTrack == i) {
            rowClasses += 'active';
          }
          return (
            <div className={rowClasses} key={s.soundcloudId} onClick={this.selectTrack} data-track-number={i}>
              <div className="playlist-row-element playlist-row-number">{i+1}</div>
              <div className="playlist-row-element playlist-row-track">{s.name}</div>
              <div className="playlist-row-element playlist-row-artist">{s.artist.name}</div>
              <div className="playlist-row-element playlist-row-soundcloud-icon">
                <a target="_blank" href={s.permalink}><img src='img/sc-logo-big.png' /></a>
              </div>
            </div>
          );
        }.bind(this))}
      </div>
    );
  }
});

//
// ReviewPanel
//

var ReviewPanel = React.createClass({
  render: function() {
    var reviewedByText = this.props.o.reviewer.name ? 'Reviewed by ' + this.props.o.reviewer.name : '';
    return (
      <div id="ReviewPanel">
        <div id="ReviewHeader">
          <div>{reviewedByText}</div>
          <div className="avatar-wrapper"><img className="reviewer-avatar" src={this.props.o.reviewer.avatar.url()} /></div>
        </div>
        <div id="ReviewBody" dangerouslySetInnerHTML={{__html: this.props.o.review.text}} />
      </div>
    );
  }
});

//
// ControlsBar
//

var ControlsBar = React.createClass({

  clickPrev: function(e) {
    e.preventDefault();
    Parse.Analytics.track('ui_event', {event: 'skip-prev'});
    this.props.o.actionPrev();
  },

  clickPlay: function(e) {
    e.preventDefault();
    if (this.props.o.playing) {
      Parse.Analytics.track('ui_event', {event: 'pause'});
    } else {
      Parse.Analytics.track('ui_event', {event: 'play'});
    }
    this.props.o.togglePlay();
  },

  clickNext: function(e) {
    e.preventDefault();
    Parse.Analytics.track('ui_event', {event: 'skip-next'});
    this.props.o.actionNext();
  },

  render: function() {
    var playButtonClass = this.props.o.playing ? 'button-pause' : 'button-play';
    return (
      <div id="ControlsBar">
        <ProgressBar position={this.props.o.position} />
        <div className='control-button button-prev' onClick={this.clickPrev} />
        <div className={'control-button ' + playButtonClass} onClick={this.clickPlay} />
        <div className='control-button button-next' onClick={this.clickNext} />
        <div id="BarTitle">
          <div id="BarTitleTrack"><p>{this.props.o.track.name}</p></div>
          <div id="BarTitleArtist"><p>{this.props.o.track.artist.name}</p></div>
        </div>
        <a target="_blank" href='https://soundcloud.com'>
          <div id="BarSoundcloudBranding">
            <div>
              <div>
                <div>powered by</div>
                <img src='img/sc-logo-big.png' />
              </div>
            </div>
          </div>
        </a>
      </div>
    );
  }
});

//
// ProgressBar
//

var ProgressBar = React.createClass({
  render: function() {
    return (
      <div id="ProgressBar"> <div id="Progress" style={{width: this.props.position + '%'}} /></div>
    );
  }
})

//
// Render
//

React.render(
  <Player />,
  document.getElementById('content')
);
