Parse.initialize('WgmeA12ET8OYVZSzhBE8k1hDxwmeNjv5RvII20vH', 'YCoITunwLprdLiucsSRYirTuefcMYpF1fe0rqeBC');
Parse.Analytics.track('view', {page: 'playlists-overview'});

var Page = React.createClass({
  mixins: [ParseReact.Mixin],

  componentDidMount: function() {},

  observe: function() {
    var playlists = (new Parse.Query('Playlist')).equalTo('isLive', true).include('creator').descending('createdAt');
    return {
      playlists: playlists,
    };
  },

  render: function() {
    return (
      <div className="container-container">
        <Header />
        {this.data.playlists.map(function(pl, i) {
          return <Playlist playlist={pl} />;
        })}
      </div>
    );
  }
});

var Header = React.createClass({

  getInitialState: function() {
    return {
      emailSaved: false,
      message: null,
    };
  },

  submit: function() {
    var self = this;

    var email = $('#email');
    if (email.val() == '' || !email[0].checkValidity()) {
      self.setState({emailSaved: false, message: 'Invalid&nbsp;email&nbsp;address!'});
      return;
    }

    self.setState({emailSaved: false, message: null});

    ParseReact.Mutation.Create('MailingListEntry', {
      email: email.val()
    }).dispatch().then(function(obj) {
      self.setState({emailSaved: true, message: 'Subscribed!'});
    }, function(error) {
      self.setState({emailSaved: false, message: 'Failed&nbsp;to&nbsp;subscribe. Please&nbsp;try&nbsp;again.'})
    });
  },

  render: function() {
    return (
      <div className="header">
        <p className="header-wrapper"><img src="img/header.png" /></p>
        <p />
        <Message emailSaved={this.state.emailSaved} message={this.state.message} />
        <p className="input-wrapper"><input id="email" type="email" placeholder="your email" /></p>
        <p><button type="submit" onClick={this.submit}>Sign&nbsp;Up</button></p>
      </div>
    );
  }
})

var Message = React.createClass({
  render: function() {
    if (this.props.message == null) {
      return <p className="input-label">Sign&nbsp;up&nbsp;for&nbsp;our&nbsp;newsletter:</p>;
    }
    var className =  'input-label ' + (this.props.emailSaved ? 'thanks' : 'error');
    return <p className={className} dangerouslySetInnerHTML={{__html: this.props.message}} />;
  }
});

var Playlist = React.createClass({
  render: function() {
    var d = this.props.playlist.publishedAt;
    var dateString = (d.getMonth() + 1).toString() + '/' + d.getDate() + '/' + d.getFullYear();
    return (
      <a href={this.props.playlist.url} className="playlist-container">
        <img alt={this.props.playlist.photoCredit} src={this.props.playlist.cover.url()} />
        <div className="caption">
          <p className="playlist-name">{this.props.playlist.name}</p>
          <p className="reviewed-by">Reviewed by {this.props.playlist.creator.name}</p>
          <p className="playlist-date">{dateString}</p>
        </div>
      </a>
    );
  }
});

React.render(
  <Page />,
  document.getElementById('content')
);
