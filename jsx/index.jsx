Parse.initialize('WgmeA12ET8OYVZSzhBE8k1hDxwmeNjv5RvII20vH', 'YCoITunwLprdLiucsSRYirTuefcMYpF1fe0rqeBC');
Parse.Analytics.track('view', {page: 'landing_preview'});

var Page = React.createClass({

  getInitialState: function() {
    return {
      emailSaved: false,
      message: null,
    };
  },

  submit: function() {
    var self = this;

    var email = $('#email');
    if (email.val() == '' || !email[0].checkValidity()) {
      self.setState({emailSaved: false, message: 'Please enter a valid email address!'});
      return;
    }

    self.setState({emailSaved: false, message: null});

    ParseReact.Mutation.Create('MailingListEntry', {
      email: email.val()
    }).dispatch().then(function(obj) {
      self.setState({emailSaved: true, message: 'Your email address was subscribed successfully!'});
    }, function(error) {
      self.setState({emailSaved: false, message: 'There was an error adding your email address. Please try again!'})
    });
  },

  render: function() {
    var page_copy = "A digital music platform dedicated to rethinking the traditional label. We put artists first and feature music you can't hear anywhere else.";
    return (
      <div className="container">
        <div className="logo-wrapper">
          <img className="logo-header" src="img/header.png" />
          <hr/>
        </div>
        <p>{page_copy}</p>

        <p>
          Sign up to be notified when we officially launch.<br/>
          <Message emailSaved={this.state.emailSaved} message={this.state.message} />
          <input id="email" type="email" name="email" placeholder="email address" />
          <button type="submit" onClick={this.submit}>Add To Mailing List</button>
        </p>
        <p style={{padding: '1rem'}}>
          Want to see preview of our streaming experience? Check out this playlist compiled by our resident music expert.
        </p>
        <p  style={{'padding-top': '1rem'}}>
          <a href="http://avium.co/playlists.html" className="button">Take Me To The Music!</a>
        </p>
      </div>
    );
  }
});

var Message = React.createClass({
  render: function() {
    if (this.props.message == null) { return <div />; }
    var className = this.props.emailSaved ? 'thanks' : 'error';
    return <div className={className}>{this.props.message}</div>;
  }
});

React.render(
  <Page />,
  document.getElementById('content')
);
