Parse.initialize('WgmeA12ET8OYVZSzhBE8k1hDxwmeNjv5RvII20vH', 'YCoITunwLprdLiucsSRYirTuefcMYpF1fe0rqeBC');
SC.initialize({client_id: 'a9375359e3a389f05ae21d1796804b57'});
Parse.Analytics.track('view', {page: 'admin'});

SC.get('/resolve', {url: "https://soundcloud.com/galanin/perfect-mistake"}, function(response) {
  debugger;
});

//
// Page
//

var Page = React.createClass({
  mixins: [ParseReact.Mixin],

  getInitialState: function() {
    return {};
  },

  observe: function() {
    var playlistQuery = (new Parse.Query('Playlist')).include('creator').include('songs.artist');
    return {
      playlists: playlistQuery,
    };
  },

  render: function() {
    return (
      <div>
        <table>
          <tr>
            <th>Playlists</th>
          </tr>
          {this.data.playlists.map(function(pl, i) {
            return (
              <tr>
                <td>{pl.objectId}</td>
                <td>{pl.name}</td>
                <td>{pl.url}</td>
                <td>{pl.isLive}</td>
                <td><a href="#edit-playlist-{pl.objectId}">Edit</a></td>
              </tr>
            );
          }.bind(this))}
        </table>
        <button onClick={this.newPlaylist}>New Playlist</button>
      </div>
    );
  }
});

//
// New Playlist Button
//

var NewPlaylistButton = React.createClass({

  getInitialState: function() {
    return {
      editingNewPlaylist: false
    };
  },

  render: function() {
    if (this.state.editingNewPlaylist == true) {
      return (
        <div>
          <button onClick={this.cancelPlaylist}>Cancel</button>
          <input type="text" placeholder="Playlist Name" />
          <button onClick={this.savePlaylist}>Save</button>
        </div>
      );
    } else {
      return (<button onClick={this.newPlaylist}>New Playlist</button>);
    }
  },

  newPlaylist: function() {
    this.setState({ editingNewPlaylist: true });
  }
});

//
// Render
//

React.render(
  <Page />,
  document.getElementById('content')
);
